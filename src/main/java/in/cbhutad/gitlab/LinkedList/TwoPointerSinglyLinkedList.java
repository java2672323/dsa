package in.cbhutad.gitlab.LinkedList;

public class TwoPointerSinglyLinkedList {
    
    static class Node {
        private int data;
        private Node next;

        public Node() {
            this.data = 0;
            this.next = null;
        }

        public Node(int data) {
            this.data = data;
            this.next = null;
        }

        public Node(int data, Node next) {
            this.data = data;
            this.next = next;
        } 

        public int getData() {
            return this.data;
        }

        public Node getNext() {
            return this.next;
        }
    }

    private Node head;
    private Node tail;

    public TwoPointerSinglyLinkedList() {
        this.head = null;
        this.tail = null;
    }

    public boolean isEmpty() {
        return this.head == null;
    }

    public void addFirst(int data) {
        Node nn = new Node(data);
        if(isEmpty()) {
            this.tail = nn;
            this.head = nn;
        } else {
            nn.next = this.head;
            this.head = nn;
        }
        
    }

    public void addLast(int data) {
        if(isEmpty()) {
            addFirst(data);
        } else {
            Node nn = new Node(data);
            tail.next = nn;
            tail = nn;
        }
    }

    public void addPos(int data, int pos) {
        if(isEmpty() || pos <= 1) {
            addFirst(data);
        } else {
            Node trav = head;
            Node nn = new Node(data);
            for(int i = 1;i < pos - 1;i++) {
                if(trav.next == null)
                    break;
                trav = trav.next;
            }
            nn.next = trav.next;
            trav.next = nn;
        }
    }

    public void deleteAll() {
        this.head = null;
    }

    public void deleteFirst() {
        if(isEmpty()) {
            //Do nothing
        } else if(head == tail) {
            head = null;
            tail = null;
        } else {
            head = head.next;
        }
    }

    public void deleteLast() {
        if(isEmpty()) {
            //Do nothing
        } else if(head == tail) {
            head = null;
            tail = null;
        } else {
            Node trav = head;
            while(trav.next != tail) {
                trav = trav.next;
            }
            tail = trav;
            tail.next = null;
        }
    }

    public void deletePos(int pos) {
        if(isEmpty() || pos <= 1) {
            deleteFirst();
        } else {
            Node trav = head;
            Node prev = null;
            for(int i = 1;i < pos;i++) {
                if(trav == null) {
                    return;
                }
                prev = trav;
                trav = trav.next;
            }
            prev.next = trav.next;
            if(trav == tail) {
                tail = prev;
            }
        }
    }

    public String display() {
        if(isEmpty()) {
            return "List is Empty";
        }
        StringBuffer sb = new StringBuffer("");
        sb.append("List elements are -> ");
        Node trav = head;
        while(trav != null) {
            sb.append(trav.data + ", ");
        }
        return sb.toString().trim();
    }
}
