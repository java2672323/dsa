package in.cbhutad.gitlab.LinkedList;

public class SinglyCircularLinkedList {

    static class Node {

        private int data;
        private Node next;

        public Node() {
            this.data = 0;
            this.next = null;
        }

        public Node(int data) {
            this.data = data;
            this.next = null;
        }

        public Node(int data, Node next) {
            this.data = data;
            this.next = next;
        }

        public int getData() {
            return this.data;
        }

        public Node getNext() {
            return this.next;
        }
    }

    private Node head;

    public Node getHead() {
        return this.head;
    }

    public SinglyCircularLinkedList() {
        this.head = null;
    }

    public boolean isEmpty() {
        return this.head == null;
    }

    public void addFirst(int data) {
        Node nn = new Node(data);
        if(isEmpty()) {
            nn.next = nn;
            head = nn;
        } else {
            Node trav = head;
            while(trav.next != head)
                trav = trav.next;
            nn.next = trav.next;
            trav.next = nn;
            head = nn;
        }
    }

    public void addLast(int data) {
        if(isEmpty()) {
            addFirst(data);
        } else {
            Node nn = new Node(data);
            Node trav = head;
            while(trav.next != head)
                trav = trav.next;
            nn.next = trav.next;
            trav.next = nn;
        }
    }

    public void addPos(int data, int pos) {
        if(isEmpty() || pos <= 1) {
            addFirst(data);
        } else {
            Node trav = head;
            Node nn = new Node(data);
            for(int i = 1;i < pos-1;i++) {
                if(trav.next == head)
                    break;
                trav = trav.next;
            }
            nn.next = trav.next;
            trav.next = nn;
        }
    }

    public void delAll() {
        this.head = null;
    }

    public void delFirst() {
        if(isEmpty()) {
            return;
        } else if(head.next == head) {
            delAll();
        } else {
            Node trav = head;
            while(trav.next != head)
                trav = trav.next;
            head = head.next;
            trav.next = head;
        }

    }

    public void delLast() {
        if(isEmpty()) {
            return;
        } else if(head.next == head) {
            delAll();
        } else {
            Node trav = head;
            while(trav.next.next != head) {
                trav = trav.next;
            }
            trav.next = trav.next.next;
        }
    }

    public void delPos(int pos) {
        if(isEmpty()) {
            return;
        } else if(pos <= 1) {
            delFirst();
        } else {
            Node trav = head;
            for(int i = 1;i < pos - 1;i++) {
                if(trav.next == head) {
                    return;
                }
                trav = trav.next;
            }
            trav.next = trav.next.next;
        }
    }
    
    public String display() {
        if(isEmpty()) {
            return "List is empty";
        } else {
            Node trav = head;
            StringBuffer sb = new StringBuffer();
            sb.append("List elements are : ");
            while(trav.next != head) {
                sb.append(trav.data + " ");
            }
            String result = sb.toString();
            return result.trim();
        }
    }

}
