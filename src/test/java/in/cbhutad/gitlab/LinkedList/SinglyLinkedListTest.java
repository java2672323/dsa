package in.cbhutad.gitlab.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import in.cbhutad.gitlab.LinkedList.SinglyLinkedList;

public class SinglyLinkedListTest {
    
    private static SinglyLinkedList linkedList;

    @BeforeEach
    private void populateList() {
        linkedList = new SinglyLinkedList();
    }

    @Test
    void testEmpty() {
        assertTrue(linkedList.isEmpty());
    }

    @Test
    void testAddFirst() {
        //Case 1: List is empty
        linkedList.addFirst(2);
        assertEquals(linkedList.getHead().getData(), 2, "Node pointed by head should have value 2");

        //Case 2: List is non-empty
        linkedList.addFirst(1);
        assertEquals(linkedList.getHead().getData(), 1, "Node pointed by head should be updated to 1");
        assertEquals(linkedList.getHead().getNext().getData(), 2, "Next Node value should be 2");
    }

    @Test
    void testAddLast() {
        //Case 1: List is empty
        linkedList.addLast(2);
        assertEquals(linkedList.getHead().getData(),2, "Node pointed by head should have value 2");

        //Case 2: List is not empty
        linkedList.addLast(3);
        linkedList.addLast(4);
        linkedList.addLast(5);
        linkedList.addLast(6);
        SinglyLinkedList.Node trav = linkedList.getHead();
        while(trav.getNext() != null) {
            trav = trav.getNext();
        }
        assertEquals(trav.getData(), 6, "Node data should be value ");

    }

    @Test
    void testAddPos() {
        //Case 1: List is empty
        linkedList.addPos(1, 0);
        assertEquals(linkedList.getHead().getData(),1, "Node pointed by head should have value 1");
        linkedList.addPos(0, 1);
        assertEquals(linkedList.getHead().getData(),0, "Node pointed by head should have value 0");

        //Case 2: List is non empty
        
    }
}
